from fpga765 import driver
import binascii
import time

if __name__ == "__main__":
    with driver.Context("COM36", 19200) as ctx, ctx.enable_drive():
        print("{0:0X}".format(*ctx.recalibrate()))
        # print("{0:0X}".format(*ctx.seek(2)))
        # print("{0:0X}".format(*ctx.seek(1)))
        # time.sleep(3)
