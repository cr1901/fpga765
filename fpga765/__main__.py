from fpga765 import platform
from fpga765.interface import Interface
from migen import *

def main():
    # TODO: Verilog options
    plat = platform.FPGA765Platform()
    en = plat.request("enable")
    leds = [plat.request("user_led") for a in range(2)]
    uart = plat.request("serial")

    m = Interface(16125000, 19200)
    fsigs = m.fdc.floppy_signals
    m.comb += [en.eq(0), # Prevent glitches from causing inadvertant writes
        # during power on.
        plat.request("REDWC").eq(fsigs.REDWC),
        fsigs.INDEX.eq(plat.request("INDEX")),
        plat.request("MOTEA").eq(fsigs.MOTEA),
        plat.request("DRVSA").eq(fsigs.DRVSA),
        plat.request("DIR").eq(fsigs.DIR),
        plat.request("STEP").eq(fsigs.STEP),
        # plat.request("WDATA").eq(fsigs.WDATA),
        # plat.request("WGATE").eq(fsigs.WGATE),
        fsigs.TRK00.eq(plat.request("TRK00")),
        fsigs.WPT.eq(plat.request("WPT")),
        fsigs.RDATA.eq(plat.request("RDATA")),
        plat.request("SIDE1").eq(fsigs.STEP),
        fsigs.DSKCHG.eq(plat.request("DSKCHG"))
    ]

    m.comb += [leds[0].eq(fsigs.WPT), leds[1].eq(fsigs.TRK00)]

    m.comb += [m.uart.rx.eq(uart.rx),
        uart.tx.eq(m.uart.tx)]

    m.clock_domains.cd_sys = ClockDomain()

    plat.build(m, run=True)
    # plat.build(m, run=False)
    prog = plat.create_programmer()
    prog.flash(0, "build/top.bin")

if __name__ == '__main__':
    main()
