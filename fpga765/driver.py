import serial
import time
import contextlib


(RECALIBRATE, READ, SEEK, SIDE, MOTOR, ENABLE, SECTOR, CURR_TRK) = range(8)
UNUSED = 0xFE # Unused is a dummy byte to detect if write queue and FDC went out of sync.
OVERRIDE = 0xFF



# FIXME: I'm unsure if this is the best way to do this. Returning self from
# a context manager appears to be bad form:
# http://stackoverflow.com/questions/1984325/explaining-pythons-enter-and-exit#comment63993006_1984346
class Context:
    def __init__(self, device, baud_rate=19200):
        self.device = device
        self.baud_rate = baud_rate

    def __enter__(self):
        self.drv = Driver(self.device, self.baud_rate)
        return self.drv

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.drv.handle.close()


# TODO: Raise DriverException on Error of any command.
class Driver:
    # TODO: Make handle?
    def __init__(self, device, baud_rate=19200):
        self.handle = serial.Serial(device, baud_rate)

    def motor_on(self):
        self.handle.write([MOTOR, 0x01])
        time.sleep(0.25) # TODO: Impl delay in hardware?
        return self.handle.read(1)

    def motor_off(self):
        self.handle.write([MOTOR, 0x00])
        return self.handle.read(1)

    def enable(self):
        self.handle.write([ENABLE, 0x01])
        return self.handle.read(1)

    def disable(self):
        self.handle.write([ENABLE, 0x00])
        return self.handle.read(1)

    def recalibrate(self):
        self.handle.write([RECALIBRATE, UNUSED])
        return self.handle.read(1)

    def seek(self, track):
        self.handle.write([SEEK, (track & 0xFF)])
        return self.handle.read(1)

    def curr_trk(self):
        self.handle.write([CURR_TRK, UNUSED])
        return self.handle.read(1)

    @contextlib.contextmanager
    def enable_drive(self):
        try:
            self.enable()
            self.motor_on()
            yield
        except Exception as e:
            raise
        finally:
            self.motor_off()
            self.disable()
