from migen import *
from migen.genlib.fifo import SyncFIFO
from fpga765.uart import Core
from fpga765.fdc import Controller

# Verilog which only depends on clock frequency can be generated using this
# module. Provides a serial port which can write to a FIFO, which is compatible
# with how the controller reads in data.
# Likewise, controller will write to memory in a FIFO fashion which can be
# read through serial port to host computer.
class Interface(Module):
    def __init__(self, clk_freq, baud_rate=115200):
        self.submodules.uart = Core(clk_freq, baud_rate)
        self.submodules.fdc = Controller(clk_freq)
        self.submodules.fifo_in = SyncFIFO(8, 1024) # In from host computer
        self.submodules.fifo_out = SyncFIFO(8, 1024)

        ###

        self.comb += [self.fifo_in.din.eq(self.uart.in_data)]
        self.comb += [self.uart.rd.eq(0), self.fifo_in.we.eq(0),
                        If(self.fifo_in.writable & ~self.uart.rx_empty,
                            self.uart.rd.eq(1),
                            self.fifo_in.we.eq(1))]

        # # FIFO
        self.comb += [self.uart.out_data.eq(self.fifo_out.dout)]
        self.comb += [self.uart.wr.eq(0), self.fifo_out.re.eq(0),
                        If(self.fifo_out.readable & self.uart.tx_empty,
                            self.uart.wr.eq(1),
                            self.fifo_out.re.eq(1))]

        self.comb += [self.fdc.in_port.din.eq(self.fifo_in.dout),
                        self.fdc.in_port.avail.eq(self.fifo_in.readable),
                        self.fifo_in.re.eq(self.fdc.in_port.do_read),
                        self.fifo_out.din.eq(self.fdc.out_port.dout),
                        self.fdc.out_port.avail.eq(self.fifo_out.writable),
                        self.fifo_out.we.eq(self.fdc.out_port.do_write)]
