from migen import *

class SeekUnit(Module):
    def __init__(self, clk_freq=16000000):
        # Data
        self.din = Signal(8)
        self.dout = Signal(8)
        self.rd = Signal(1)
        self.wr = Signal(1)

        # Control
        self.recal = Signal(1)
        self.seek = Signal(1)
        self.done = Signal(1)
        # self.en provided by fsm.ce

        # IO
        self.STEP = Signal(1)
        self.DIR = Signal(1)
        self.TRK00 = Signal(1)
        self.STEP.reset = 1
        self.DIR.reset = 1

        # Shared
        self.curr_trk = Signal(8) # Shared with ReadUnit.

        # self.submodules.seek_timer = \
        #     Timer(clk_freq, 6) # 6ms track to track.
        self.submodules.seek_timer = \
            Timer(clk_freq, 1000) # Debugging.
        self.submodules.step_timer = \
            Timer(clk_freq, 0.007) # 7us... from NEC 765 datasheet.
        self.submodules.fsm = fsm = CEInserter()(FSM("IDLE"))

        status = Signal(8)
        target_trk = Signal(8)
        curr_trk = Signal(8)
        trk_cnt = Signal(max=77)
        do_wait = Signal(1)
        do_step = Signal(1)
        seek_forward = Signal(1)

        ###

        # do_step/do_wait are one cycle longer than they should be.
        self.comb += [self.step_timer.wait.eq(do_step)]
        self.comb += [self.seek_timer.wait.eq(do_wait)]
        self.comb += [seek_forward.eq(target_trk > curr_trk)]

        fsm.act("IDLE",
            If(self.recal, NextState("RECAL"),
                NextValue(trk_cnt, 77)).
            Elif(self.seek, NextState("CALC"),
                NextValue(target_trk, self.din)).
            Elif(self.curr_trk,
                NextValue(status, curr_trk),
                NextState("DONE"))
        )

        fsm.act("RECAL",
            self.DIR.eq(1),
            self.STEP.eq(do_step),

            If(~self.TRK00,
                NextValue(status, 0x00),
                curr_trk.eq(0),
                NextState("DONE")
            ).
            Elif(trk_cnt == 0,
                NextValue(status, 0x10),
                NextState("DONE")
            ).
            Else(
                # FIXME: This is ugly, but we shouldn't bother waiting when
                # we've just entered recalibration.
                If(self.seek_timer.elapsed | fsm.after_entering("RECAL"),
                    NextValue(do_step, 1),
                    NextValue(do_wait, 0)
                ),

                If(self.step_timer.elapsed,
                    NextValue(trk_cnt, trk_cnt - 1),
                    NextValue(do_step, 0),
                    NextValue(do_wait, 1)
                )
            )
        )

        fsm.act("CALC",
            If(seek_forward,
                NextValue(trk_cnt, target_trk - curr_trk)
            ).
            Else(
                NextValue(trk_cnt, curr_trk - target_trk)
            ),
            NextState("SEEK")
        )

        fsm.act("SEEK",
            self.DIR.eq(~seek_forward), # Active-Low
            self.STEP.eq(do_step),

            If(trk_cnt == 0,
                NextValue(status, 0x00),
                NextValue(curr_trk, target_trk),
                NextState("DONE") # NextState will be verify track.
            ).
            Else(
                # FIXME: This is ugly, but we shouldn't bother waiting when
                # we've just entered recalibration.
                If(self.seek_timer.elapsed | fsm.after_entering("SEEK"),
                    NextValue(do_step, 1),
                    NextValue(do_wait, 0)
                ),

                If(self.step_timer.elapsed,
                    NextValue(trk_cnt, trk_cnt - 1),
                    NextValue(do_step, 0),
                    NextValue(do_wait, 1)
                )
            )
        )

        # TODO: when done is asserted, busy will deassert next cycle.
        # Thus, SeekUnit will always be IDLE when busy is asserted.
        # Prove that this is true.

        # The CmdUnit will ensure that do_write is asserted only when
        # out queue is available.
        fsm.act("DONE",
            self.dout.eq(status),
            self.wr.eq(1),
            self.rd.eq(1), # We are done with the value.
            self.done.eq(1), NextState("IDLE")
        )


# Interval in milliseconds.
# clk_freq in Hz.
class Timer(Module):
    def __init__(self, clk_freq=16000000, interval=200):
        self.wait = Signal(1)
        self.elapsed = Signal(1)
        count_target = int((interval / 1000) * clk_freq)
        print(count_target)
        counter = Signal(max=count_target)

        self.comb += [self.elapsed.eq(counter == 0)]
        self.sync += [
            If(self.wait,
                counter.eq(counter - 1)
            ).
            Else(
                counter.eq(count_target)
            )
        ]
