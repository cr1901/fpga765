from migen import *
from migen.build.generic_platform import *
from migen.build.platforms import icestick

# fdc_record = Record([
#
# ])


fdc_io = [
        ("enable", 0, None), # W
        ("REDWC", 0, None), # W
        ("INDEX", 0, None), # R
        ("MOTEA", 0, None), # W
        ("DRVSA", 0, None), # W
        ("DIR", 0, None), # W
        ("STEP", 0, None), # W
        ("WDATA", 0, None), # W
        ("WGATE", 0, None), # W
        ("TRK00", 0, None), # R
        ("WPT", 0, None), # R
        ("RDATA", 0, None), # R
        ("SIDE1", 0, None), # W
        ("DSKCHG", 0, None), # R
        # ("user_led", 0, None),
        # ("user_led", 1, None),
        # ("user_led", 2, None),
        # ("user_led", 3, None),
        # ("serial", 0,
        #    Subsignal("tx", None),
        #    Subsignal("rx", None)
        # )
]

# IceStick-specific
class PullupGPIO(Module):
    def __init__(self, *args):
        self.out = Signal(len(*args))
        for i, a in enumerate(*args):
            # print(i, a)
            self.specials += Instance("SB_IO",
                p_PULLUP=C(1),
                p_PINTYPE=C(0),
                io_PACKAGE_PIN=a,
                i_INPUT_CLK=ClockSignal(),
                o_D_IN_0=self.out[i]
            )

class SysClkPLL(Module):
    def __init__(self):
        self.clock_domains.cd_in = ClockDomain("in")
        sys_clk = ClockSignal("sys")

        self.specials.pll = Instance("SB_PLL40_CORE",
            i_REFERENCECLK=self.cd_in.clk,
            i_RESETB=~self.cd_in.rst,
            o_PLLOUTGLOBAL=Signal(1),
            p_FILTER_RANGE=C(1, 3),
            p_DIVR=0,
            p_DIVF=85,
            p_DIVQ=6)

        self.specials.gbuf = Instance("SB_GB",
            i_USER_SIGNAL_TO_GLOBAL_BUFFER=self.pll.get_io("PLLOUTGLOBAL"),
            o_GLOBAL_BUFFER_OUTPUT=sys_clk)



# FIXME: This is really not the way to abstract away platforms...
class FPGA765Platform(GenericPlatform):
    def __init__(self, platform="icestick"):
        self.routing = Module() # Platform-specific logic. Added at the end.
        if platform == "icestick":
            icestick.Platform.__init__(self)
            self.add_fdc_ios(["GPIO0:7",
                "GPIO0:6",
                "GPIO1:7",
                "GPIO0:5",
                "GPIO0:4",
                "GPIO0:3",
                "GPIO0:2",
                "GPIO0:1",
                "GPIO0:0",
                "GPIO1:6",
                "GPIO1:5",
                "GPIO1:4",
                "GPIO1:3",
                "GPIO1:2"])

            self.build_fcn = icestick.Platform.build
            self.create_prog = icestick.Platform.create_programmer
            pll = SysClkPLL()
            self.routing.submodules += pll
            self.routing.comb += [pll.cd_in.clk.eq(self.request("clk12"))]
            self.toolchain.pnr_opt = ""
            # icestick.Platform.add_period_constraint(self, )

    def add_fdc_ios(self, *args):
        ios = [(io[0], io[1], Pins(pin)) for pin, io in zip(*args, fdc_io)]
        self.add_extension(ios)

    def request(self, *args, **kwargs):
        sig = GenericPlatform.request(self, *args, **kwargs)
        if args[0] in ["INDEX", "RDATA", "WPT", "TRK00", "DSKCHG"]:
            pullup = PullupGPIO(sig)
            sig = pullup.out[0]
            self.routing.submodules += pullup
        # elif args[0] == "clk":
        #    self.routing.comb += [sig.eq(ClockSignal("sys"))]
        return sig

    def build(self, *args, **kwargs):
        return self.build_fcn(self, *args, **kwargs)

    def create_programmer(self, *args, **kwargs):
        return self.create_prog(self, *args, **kwargs)

    def do_finalize(self, f, *args, **kwargs):
        f += self.routing.get_fragment()
        GenericPlatform.do_finalize(self, f, *args, **kwargs)
