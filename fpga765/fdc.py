from migen import *
from fpga765.driver import READ, SEEK, SIDE, MOTOR, ENABLE, RECALIBRATE, \
    SECTOR, CURR_TRK
from fpga765.seek import SeekUnit

# The minimum verilog module. data_out and data_in expect to be attached to a
# queue of at least 1024+3 bytes of some sort (synchronous should be fine).

# Format of write is: CMD, PARAM (may be null), PAYLOAD (may be null if PARAM != null).
# ID        CMD     PARAM       PAYLOAD
# 0x01      SEEK    TRACK_NO    NONE
# 0x02      SIDE    SIDE_NO     NONE
# 0x03      READ    SECTOR_NO   512 bytes
# Format of read is: [PAYLOAD] STATUS
# Status bits:
# 0x40- Not Implemented.


@CEInserter()
class SimpleCtrl(Module):
    def __init__(self, width=1):
        self.i = Signal(width)
        self.o = Signal(width)
        self.o.reset = 1

        ###

        self.sync += [self.o.eq(self.i)]


class Controller(Module):
    def __init__(self, clk_freq):
        self.in_port = Record(_fdc_in)
        self.out_port = Record(_fdc_out)
        self.floppy_signals = sigs = Record(_io_signals)

        self.submodules.cmd_unit = CmdUnit()
        # Units to handle simple commands
        self.submodules.side1 = SimpleCtrl()
        self.submodules.drive_select = SimpleCtrl()
        self.submodules.motor_ctrl = SimpleCtrl()
        self.submodules.seek = SeekUnit(clk_freq)

        sigs.REDWC.reset=1
        sigs.WDATA.reset=1
        sigs.WGATE.reset=1

        ###

        self.comb += [self.cmd_unit.bus_in.connect(self.in_port),
            self.cmd_unit.bus_out.connect(self.out_port)]

        # Don't bother making a record just to propogate the signals; units
        # can't stand on their own. Naming convention is reversed from Records;
        # out connects to in.
        self.comb += [self.drive_select.i.eq(self.cmd_unit.drvsel_dout),
            self.drive_select.ce.eq(self.cmd_unit.drvsel_en),
            sigs.DRVSA.eq(self.drive_select.o)]

        self.comb += [self.motor_ctrl.i.eq(self.cmd_unit.mctrl_dout),
            self.motor_ctrl.ce.eq(self.cmd_unit.mctrl_en),
            sigs.MOTEA.eq(self.motor_ctrl.o)]

        self.comb += [self.side1.i.eq(self.cmd_unit.side1_dout),
            self.side1.ce.eq(self.cmd_unit.side1_en),
            sigs.SIDE1.eq(self.side1.o)]

        # SeekUnit
        self.comb += [
            self.seek.din.eq(self.cmd_unit.seek_din),
            self.seek.recal.eq(self.cmd_unit.seek_recal),
            self.seek.seek.eq(self.cmd_unit.seek_seek),
            self.seek.curr_trk.eq(self.cmd_unit.seek_curr_trk),
            self.seek.fsm.ce.eq(self.cmd_unit.seek_en),
            self.cmd_unit.seek_dout.eq(self.seek.dout),
            self.cmd_unit.seek_done.eq(self.seek.done),
            self.cmd_unit.seek_wr.eq(self.seek.wr),
            self.cmd_unit.seek_rd.eq(self.seek.rd),
            sigs.DIR.eq(self.seek.DIR),
            sigs.STEP.eq(self.seek.STEP),
            self.seek.TRK00.eq(sigs.TRK00)]

        # ReadUnit


_io_signals = [
    ("REDWC", 1, DIR_M_TO_S), # W
    ("INDEX", 1, DIR_S_TO_M), # R
    ("MOTEA", 1, DIR_M_TO_S), # W
    ("DRVSA", 1, DIR_M_TO_S), # W
    ("DIR", 1, DIR_M_TO_S), # W
    ("STEP", 1, DIR_M_TO_S), # W
    ("WDATA", 1, DIR_M_TO_S), # W
    ("WGATE", 1, DIR_M_TO_S), # W
    ("TRK00", 1, DIR_S_TO_M), # R
    ("WPT", 1, DIR_S_TO_M), # R
    ("RDATA", 1, DIR_S_TO_M), # R
    ("SIDE1", 1, DIR_M_TO_S), # W
    ("DSKCHG", 1, DIR_S_TO_M), # R
]

_fdc_in = [
    ("avail", 1, DIR_S_TO_M),
    ("do_read", 1, DIR_M_TO_S),
    ("din", 8, DIR_S_TO_M),
]

_fdc_out = [
    ("avail", 1, DIR_S_TO_M),
    ("do_write", 1, DIR_M_TO_S),
    ("dout", 8, DIR_M_TO_S)
]

_fdc_unit = [
    ("din", 8, DIR_S_TO_M),
    ("rd", 1, DIR_S_TO_M),
    ("wr", 1, DIR_S_TO_M),
    ("dout", 8, DIR_M_TO_S),
    ("done", 1, DIR_S_TO_M),
    ("en", 1, DIR_M_TO_S)
]


class CmdUnit(Module):
    def __init__(self):
        self.bus_in = Record(_fdc_in)
        self.bus_out = Record(_fdc_out)

        # FIXME: Abuse of Records?
        self.side1_dout = Signal(1)
        self.side1_en = Signal(1)

        self.drvsel_dout = Signal(1)
        self.drvsel_en = Signal(1)

        self.mctrl_dout = Signal(1)
        self.mctrl_en = Signal(1)

        self.seek_dout = Signal(8)
        self.seek_din = Signal(8)
        self.seek_wr = Signal(1)
        self.seek_rd = Signal(1)
        self.seek_done = Signal(1)
        self.seek_en = Signal(1)
        self.seek_seek = Signal(1)
        self.seek_recal = Signal(1)
        self.seek_curr_trk = Signal(1)

        busy = Signal(1)
        cmd = Signal(8)
        done = Signal(1) # Can be driven by a constant or a register. busy
        # synchronizes itself to assertion of done.


        idle_op_comb = [If(self.bus_in.avail,
                      self.bus_in.do_read.eq(1))]

        idle_op_sync = [If(self.bus_in.avail,
            cmd.eq(self.bus_in.din),
            busy.eq(1))
        ]

        simple_bus_ops = [
            self.bus_in.do_read.eq(1),
            self.bus_out.dout.eq(0x00),
            done.eq(1),
            self.bus_out.do_write.eq(1)
        ]

        seek_bus_ops = [

        ]

        unit_mux = Case(cmd, {
            ENABLE : [
                # To keep busy/done interface simple, wait until both in/out
                # are available. That way, we don't need a third state for
                # waiting for write unit to free up.
                # Simple commands assert done immediately. More complex
                # operations will wait for done from the enabled unit.
                # Units should maintain previous state if the queues aren't available.
                If(self.bus_in.avail & self.bus_out.avail,
                    self.drvsel_dout.eq(~self.bus_in.din[0]),
                    self.drvsel_en.eq(1),
                    simple_bus_ops)
            ],

            MOTOR : [
                If(self.bus_in.avail & self.bus_out.avail,
                    self.mctrl_dout.eq(~self.bus_in.din[0]),
                    self.mctrl_en.eq(1),
                    simple_bus_ops)
            ],

            SIDE : [
                If(self.bus_in.avail & self.bus_out.avail,
                    self.side1_dout.eq(~self.bus_in.din[0]),
                    self.side1_en.eq(1),
                    simple_bus_ops)
            ],

            # TODO: Does not routing unused signals have meaningful
            # impact on resource usage?
            RECALIBRATE : [
                If(self.bus_in.avail & self.bus_out.avail,
                    self.bus_out.dout.eq(self.seek_dout),
                    self.bus_out.do_write.eq(self.seek_wr),
                    self.bus_in.do_read.eq(self.seek_rd),
                    self.seek_en.eq(1),
                    done.eq(self.seek_done),
                    self.seek_din.eq(self.bus_in.din),
                    self.seek_recal.eq(1))
            ],

            SEEK : [
                If(self.bus_in.avail & self.bus_out.avail,
                    self.bus_out.dout.eq(self.seek_dout),
                    self.bus_out.do_write.eq(self.seek_wr),
                    self.bus_in.do_read.eq(self.seek_rd),
                    self.seek_en.eq(1),
                    done.eq(self.seek_done),
                    self.seek_din.eq(self.bus_in.din),
                    self.seek_seek.eq(1))
            ],

            CURR_TRK : [
                If(self.bus_in.avail & self.bus_out.avail,
                    self.bus_out.dout.eq(self.seek_dout),
                    self.bus_out.do_write.eq(self.seek_wr),
                    self.bus_in.do_read.eq(self.seek_rd),
                    self.seek_en.eq(1),
                    done.eq(self.seek_done),
                    self.seek_din.eq(self.bus_in.din),
                    self.seek_curr_trk.eq(1))
            ],

            "default" :
                [If(self.bus_out.avail,
                    self.bus_out.dout.eq(0x40),
                    done.eq(1),
                    self.bus_out.do_write.eq(1))]
            }
        )

        self.comb += [self.bus_in.do_read.eq(0),
                      self.bus_out.do_write.eq(0),
                      If(busy, unit_mux).
                      Else(idle_op_comb)]

        self.sync += [If(~busy, idle_op_sync).
                      Elif(done, busy.eq(0))]
